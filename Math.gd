extends Node
class_name Math


static func cmpflt(a, b, epsilon = 0.00001):
	return abs(a - b) <= epsilon

static func normalizeAngle(angle: float):
	var count = int(angle / TAU)
	var base = count * TAU
	var normalizedAngle = angle - base
	return {"angle": normalizedAngle, "base": base, "count": count}

# Found Inverse Hyperbolic Functions here:
# * http://wwwf.imperial.ac.uk/metric/metric_public/functions_and_graphs/hyperbolic_functions/inverses.html
static func acosh(ratio: float):
	# Note: log is the natural Log
	return log(ratio + sqrt(ratio * ratio - 1))
