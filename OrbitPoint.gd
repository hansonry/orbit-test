extends FlightPathPoint
class_name OrbitPoint


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var radialUnitDirection : Vector3
var transversalUnitDirection : Vector3 # Velocity Direction
var outOfPlaneUnitDirection : Vector3 # Not sure what this is
var unitVelocity            : Vector3



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func compute(orbit, anomolyAngle: float):
	value = anomolyAngle
	flightPathChunk = orbit
	var u = orbit.argumentOfPeriapsis + anomolyAngle
	var sinOfU = sin(u)
	var cosOfU = cos(u)
	var sinOfOmega = sin(orbit.omega)
	var cosOfOmega = cos(orbit.omega)
	var sinOfInclination = sin(orbit.inclinationAngle)
	var cosOfInclination = cos(orbit.inclinationAngle)
	
	var velAngleOffset
	match orbit.type:
		orbit.ORBITTYPE_CIRCULAR:
			velAngleOffset = 0
		orbit.ORBITTYPE_ELLIPTICAL, orbit.ORBITTYPE_HYPERBOLIC:
			velAngleOffset = atan2(orbit.eccentricity * sin(anomolyAngle), 1 + orbit.eccentricity * cos(anomolyAngle))
		orbit.ORBITTYPE_PARABOLIC:
			velAngleOffset = anomolyAngle / 2
		_:
			velAngleOffset = 0 
	var va =  u + PI / 2 - velAngleOffset
	var sinOfVa = sin(va)
	var cosOfVa = cos(va)

	unitVelocity.x = cosOfVa * cosOfOmega - sinOfVa * sinOfOmega * cosOfInclination
	unitVelocity.z = -cosOfVa * sinOfOmega - sinOfVa * cosOfOmega * cosOfInclination
	unitVelocity.y = sinOfVa * sinOfInclination

	radialUnitDirection.x = cosOfU * cosOfOmega - sinOfU * sinOfOmega * cosOfInclination
	radialUnitDirection.z = -cosOfU * sinOfOmega - sinOfU * cosOfOmega * cosOfInclination
	radialUnitDirection.y = sinOfU * sinOfInclination
	
	transversalUnitDirection.x = -sinOfU * cosOfOmega - cosOfU * sinOfOmega * cosOfInclination
	transversalUnitDirection.z = sinOfU * sinOfOmega - cosOfU * cosOfOmega * cosOfInclination
	transversalUnitDirection.y = cosOfU * sinOfInclination
	
	outOfPlaneUnitDirection.x = sinOfOmega * sinOfInclination
	outOfPlaneUnitDirection.z = cosOfOmega * sinOfInclination
	outOfPlaneUnitDirection.y = cosOfInclination
	
	var radius
	match orbit.type:
		orbit.ORBITTYPE_CIRCULAR:
			radius = orbit.semiMajorAxis
		orbit.ORBITTYPE_ELLIPTICAL, orbit.ORBITTYPE_HYPERBOLIC:
			var cosOfAngleMEP1 = 1 + orbit.eccentricity * cos(anomolyAngle)
			if Math.cmpflt(cosOfAngleMEP1, 0):
				radius = NAN
			else:
				radius = orbit.semiLatusRectum / cosOfAngleMEP1
		orbit.ORBITTYPE_PARABOLIC:
			var onePCOSOfAngle = 1 + cos(anomolyAngle)
			if Math.cmpflt(onePCOSOfAngle, 0):
				radius = NAN
			else:
				radius = 2 * orbit.pariapsisDistance / onePCOSOfAngle
	
	var cosOfAngleMEP1 = 1 + orbit.eccentricity * cos(anomolyAngle)

	if Math.cmpflt(cosOfAngleMEP1, 0):
		radius = 0
	else:
		radius = orbit.semiMajorAxis * (1 - orbit.eccentricity * orbit.eccentricity) / cosOfAngleMEP1
	position = radialUnitDirection * radius
	

	var velMag 
	match orbit.type:
		orbit.ORBITTYPE_CIRCULAR: 
			velMag = sqrt(orbit.gravitationalParameter * (1 / orbit.semiMajorAxis))
		orbit.ORBITTYPE_ELLIPTICAL, orbit.ORBITTYPE_HYPERBOLIC:
			velMag = sqrt(orbit.gravitationalParameter * (2 / radius - 1 / orbit.semiMajorAxis))
		orbit.ORBITTYPE_PARABOLIC:
			velMag = sqrt(orbit.gravitationalParameter * (2 / radius))
	velocity = unitVelocity * velMag
