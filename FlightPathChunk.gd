extends Spatial
class_name FlightPathChunk

var isLoop     : bool
var startValue : float
var endValue   : float
var pathTime   : float

func get_at(value: float): 
	assert(false)
	return null

func get_travel_time(value0 : float, value1 : float):
	assert(false)
	return 0

func get_travel_value(value0: float, delta_time : float):
	assert(false)
	return value0
