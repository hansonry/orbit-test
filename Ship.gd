extends RigidBody

const RenderableOrbit = preload("res://RenderableOrbit.tscn")
const clearToRailTimeout : float = 1.0 # Seconds

const ctrlThrottleSpeed : float = 2.0

# Declare member variables here:
export(NodePath) var orbitTargetPath : NodePath
export(Vector3) var initVel = Vector3.ZERO
export(bool) var controlled = false
var orbitTargetNode : Spatial
var orbit
var onRails: bool = false
var clearToRailTimer: float = clearToRailTimeout
var stored_angluar_velocity: Vector3
var railsOffArea : Area = null
var orbitAffected : bool = true

# Controls
var throttle : float = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	orbit = RenderableOrbit.instance()
	linear_velocity = initVel
	var orbitTargetNode = get_node(orbitTargetPath)
	set_orbit_target_node(orbitTargetNode)
	railsOffArea = get_node(@"RailsOffArea")

func toggle_rails():
	if onRails:
		take_off_rails()
	else:
		put_on_rails()

func put_on_rails():
	if not onRails:
		stored_angluar_velocity = angular_velocity
		orbitAffected = true
		recompute_orbit()
		mode = MODE_STATIC
		onRails = true
		print("Ship: ", self, ", Rails: Activated")

func take_off_rails():
	if onRails:
		mode = MODE_RIGID
		linear_velocity = orbit.get_current_orbit_velocity()
		angular_velocity = stored_angluar_velocity
		onRails = false
		print("Ship: ", self, ", Rails: Deactivated")

func _dropRails():
	if onRails:
		take_off_rails()
	clearToRailTimer = clearToRailTimeout
	

func _input(ev):
	if ev is InputEventKey and ev.scancode == KEY_R and not ev.echo and ev.pressed:
		toggle_rails()


func  _integrate_forces( state ):
	if not onRails:
		var positionDiff = (orbitTargetNode.get_global_transform().origin - get_global_transform().origin)
		var forceMagnatude = orbitTargetNode.gravitationParameter / positionDiff.length_squared()
		var force = positionDiff.normalized() * forceMagnatude
		state.add_force(force, Vector3.ZERO)
	if throttle > 0:
		state.add_force(Vector3.FORWARD * throttle, Vector3.ZERO)
		orbitAffected = true
	


func recompute_orbit():
	if orbitAffected:
		orbit.compute_from_ship(self)
		orbitAffected = false

func set_orbit_target_node(orbitTargetNode : Spatial):
	self.orbitTargetNode = orbitTargetNode
	orbitTargetNode.add_child(orbit)
	recompute_orbit()

func _physics_process(delta):
	var railsOffHits = railsOffArea.get_overlapping_bodies()
	var railsOffHitsCount = railsOffHits.size()
	if railsOffHitsCount > 0 and (railsOffHitsCount != 1 or railsOffHits[0] != self):
		_dropRails()
	
	if clearToRailTimer < delta:
		clearToRailTimer = 0
	else:
		clearToRailTimer -= delta
	
	if clearToRailTimer <= 0 and not onRails:
		put_on_rails()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if onRails:
		translation = orbitTargetNode.get_global_transform().origin + orbit.interpolate_current_position()
		rotation += stored_angluar_velocity * delta
	else:
		recompute_orbit()
	
	if controlled:
		if Input.is_action_pressed("ctrl_throttle_up"):
			throttle += delta * ctrlThrottleSpeed
		elif Input.is_action_pressed("ctrl_throttle_down"):
			throttle -= delta * ctrlThrottleSpeed
	
	if throttle > 1:
		throttle = 1.0
	elif throttle < 0:
		throttle = 0.0
	
	if throttle > 0:
		_dropRails()
	
