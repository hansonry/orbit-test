extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var pressed = false


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _input(ev):
	if ev is InputEventMouseButton:
		if ev.button_index == BUTTON_MIDDLE:
			pressed = ev.pressed
		elif ev.is_pressed():
			if ev.button_index == BUTTON_WHEEL_UP:
				scale_object_local  (Vector3(0.9, 0.9, 0.9))
				pass
			elif ev.button_index == BUTTON_WHEEL_DOWN:
				scale_object_local  (Vector3(1.1, 1.1, 1.1))
				pass
	if ev is InputEventMouseMotion:
		if pressed:
			rotate_z(deg2rad(ev.relative.x))
			rotate_x(deg2rad(-ev.relative.y))
			
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
