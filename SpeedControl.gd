extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var timeScale = 1


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var tree = get_tree()
	if Input.is_action_just_pressed("sim_speed_pause"):
		tree.paused = not tree.paused
		if tree.paused:
			Engine.set_time_scale(1)
		else:
			Engine.set_time_scale(timeScale)
	elif not tree.paused:
		if Input.is_action_just_pressed("sim_speed_normal"):
			Engine.set_time_scale(1)
			timeScale = 1
		elif Input.is_action_just_pressed("sim_speed_fast1"):
			Engine.set_time_scale(2)
			timeScale = 2
		elif Input.is_action_just_pressed("sim_speed_fast2"):
			Engine.set_time_scale(4)
			timeScale = 4
		elif Input.is_action_just_pressed("sim_speed_fast3"):
			Engine.set_time_scale(8)
			timeScale = 8
	pass
