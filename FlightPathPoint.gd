extends Node
class_name FlightPathPoint

var value    : float
var position : Vector3
var velocity : Vector3
var flightPathChunk
