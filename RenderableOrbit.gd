extends Orbit


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var orbitPathNode : ImmediateGeometry
var orbitInterpolateTable = OrbitInterpolateTable.new()

# Time Table computation


# Called when the node enters the scene tree for the first time.
func _ready():
	orbitPathNode = get_node(@"OrbitPath")


func _updateOrbitPath():
	orbitPathNode.clear()
	#orbitPathNode.begin(Mesh.PRIMITIVE_LINE_LOOP)
	orbitPathNode.begin(Mesh.PRIMITIVE_LINES)
	#orbitPathNode.begin(Mesh.PRIMITIVE_POINTS)
	for entry in orbitInterpolateTable.timeTable:
		var obp = entry.orbitPoint.position
		orbitPathNode.add_vertex(obp)
	orbitPathNode.end()

func compute_from_ship(ship: RigidBody):
	var largeBody = get_parent()
	var position = (ship.get_global_transform().origin - largeBody.get_global_transform().origin )
	compute(largeBody.gravitationParameter, position, ship.linear_velocity)

func compute(gravitationalParameter : float, position : Vector3, velocity: Vector3):
	.compute(gravitationalParameter, position, velocity)
	orbitInterpolateTable.new_orbit(self)
	_updateOrbitPath()

func interpolate_current_position():
	return orbitInterpolateTable.interpolate_current_position()

func get_current_orbit_position():
	return orbitInterpolateTable.get_current_position()

func get_current_orbit_velocity():
	return orbitInterpolateTable.get_current_velocity()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	orbitInterpolateTable.update(delta)
