extends Node
class_name OrbitInterpolateTable


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var orbit: Orbit = null
var timeTable = []
var currentOrbitTime : float
var currentOrbitPoint : OrbitPoint = OrbitPoint.new()
var currentOrbitPointComputed : bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _center_angle_focus_angle(angle: float):
	var c = orbit.eccentricity * orbit.semiMajorAxis
	var b = sqrt(1 - pow(orbit.eccentricity, 2)) * orbit.semiMajorAxis
	var r = b / sqrt(1 - pow(orbit.eccentricity * cos(angle), 2))
	var ang = atan2(r * sin(angle), r * cos(angle) - c)
	if ang < 0:
		ang = TAU + ang
	return ang

func new_orbit(orbit: Orbit, division: int = 64):
	self.orbit = orbit
	timeTable.clear()
	currentOrbitTime = orbit.trueAnomalyTime
	#orbit.test_mean_anomaly_func()
	if orbit.type == Orbit.ORBITTYPE_HYPERBOLIC:
		var infAngle = acos(-1 / orbit.eccentricity)
		var deltaA = infAngle * 2 / float(division)
		for i in range(0, division):
			var angle = i * deltaA
			if angle > infAngle:
				angle += TAU - 2 * infAngle
			if angle < infAngle or angle > TAU - infAngle:
				#print("Angle: ", angle, ", a2: ", a2)
				var ttEntry = { "angle": angle }
				timeTable.push_back(ttEntry)
	else:
		var deltaA = TAU / float(division)
		for i in range(0, division):
			var angle = i * deltaA
			var a2
			if orbit.type == Orbit.ORBITTYPE_ELLIPTICAL:
				a2 = _center_angle_focus_angle(angle)
			else:
				a2 = angle
			#print("Angle: ", angle, ", a2: ", a2)
			var ttEntry = { "angle": a2 }
			timeTable.push_back(ttEntry)
	for entry in timeTable:
		entry.time = orbit.get_travel_time(0, entry.angle)
		var orbitPoint = OrbitPoint.new()
		orbitPoint.compute(orbit, entry.angle)
		entry.orbitPoint = orbitPoint

func _compute_current_point_if_nessary():
	if not currentOrbitPointComputed and orbit != null:
		var trueAnomaly = orbit.get_travel_value(0, currentOrbitTime)
		currentOrbitPoint.compute(orbit, trueAnomaly)
		currentOrbitPointComputed = true

func _normalize_time(time: float):
	var n = {"time": 0, "count": 0, "base": 0}
	if orbit == null:
		return n
	n.count = int(time / orbit.period)
	n.base = n.count * orbit.period
	n.time = time - n.base
	return n

func _interpolate_position_from_time(time: float):
	# Search for a larger Time
	assert(time >= 0)
	var foundIndex : int = -1
	var index : int = 0
	for entry in timeTable:
		if time < entry.time:
			foundIndex = index
			break
		index += 1
	assert(foundIndex != 0)
	var entityBefore
	var entityAfter
	var percent : float
	if index < 0:
		# We didn't find a match, so it is after the last one
		entityBefore = timeTable[-1]
		entityAfter = timeTable[0]
		percent = (time  - entityBefore.time) / (orbit.period - entityBefore.time)
	else:
		entityBefore = timeTable[foundIndex - 1]
		entityAfter = timeTable[foundIndex]
		percent = (time  - entityBefore.time) / (entityAfter.time - entityBefore.time)
	return entityBefore.orbitPoint.position.linear_interpolate(entityAfter.orbitPoint.position, percent)
  

func interpolate_current_position():
	var interpTime
	match orbit.type:
		Orbit.ORBITTYPE_CIRCULAR, Orbit.ORBITTYPE_ELLIPTICAL:
			var nTime = _normalize_time(currentOrbitTime)
			interpTime = nTime.time
		_:
			interpTime = currentOrbitTime
	return _interpolate_position_from_time(interpTime)

func get_current_position():
	_compute_current_point_if_nessary()
	return currentOrbitPoint.position

func get_current_velocity():
	_compute_current_point_if_nessary()
	return currentOrbitPoint.velocity

func update(delta):
	currentOrbitPointComputed = false
	currentOrbitTime += delta

