extends FlightPathChunk
class_name LinearFlightPathChunk

var startPos : Vector3
var velocity : Vector3

func compute(position : Vector3, velocity: Vector3, lengthOfTime : float):
	isLoop = false
	startPos = position
	self.velocity = velocity
	startValue = 0
	endValue = lengthOfTime
	pathTime = lengthOfTime

func get_at(value: float): 
	return startPos + velocity * value

func get_travel_time(value0 : float, value1 : float):
	return value1 - value0

func get_travel_value(value0: float, delta_time : float):
	return value0 + delta_time
