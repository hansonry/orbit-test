extends FlightPathChunk
class_name Orbit
# This assumes mass of satellite is negligible
# Refrences and sources:
# * http://www.braeunig.us/space/orbmech.htm
# * https://space.stackexchange.com/questions/1904/how-to-programmatically-calculate-orbital-elements-using-position-velocity-vecto
# * https://github.com/RazerM/orbital/blob/0.7.0/orbital/utilities.py#L252
# Sources of non-elipical orbits (hyperbolic and parabolic)
# * http://www.bogan.ca/orbits/kepler/orbteqtn.html
# * https://space.stackexchange.com/questions/23128/design-of-an-elliptical-transfer-orbit/23129#23129
# All angles in class members will be in radians

enum {
	ORBITTYPE_UNKNOWN, # This shouldn't be the type if you computed the orbit
	ORBITTYPE_CIRCULAR, 
	ORBITTYPE_ELLIPTICAL, 
	ORBITTYPE_PARABOLIC, 
	ORBITTYPE_HYPERBOLIC
}

var gravitationalParameter : float
var eccentricity           : float
var inclinationAngle       : float
var semiMajorAxis          : float
var omega                  : float
var argumentOfPeriapsis    : float
var pariapsisDistance      : float
var meanMotion             : float
var period                 : float
var semiLatusRectum        : float
var type = ORBITTYPE_UNKNOWN

# We are going to assume Periapsis is always 0 degrees
# and Apoapsis is always 180 degrees
var trueAnomalyAngle # AKA the thing orbiting
var trueAnomalyTime

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _computePeriod():
	match type:
		ORBITTYPE_CIRCULAR, ORBITTYPE_ELLIPTICAL:
			var meanAnomoly0 = compute_mean_anomaly_from_true_anomaly(0)
			var meanAnomoly1 = compute_mean_anomaly_from_true_anomaly(TAU)
			return (meanAnomoly1 - meanAnomoly0) / meanMotion
		_:
			return NAN

func _compute_orbital_parameters(gravitationalParameter : float, position : Vector3, velocity: Vector3):
	self.gravitationalParameter = gravitationalParameter
	var angularMomentum = position.cross(velocity)
	var nodeVector = Vector3(0, 1, 0).cross(angularMomentum)
	var nodeVectorLength = nodeVector.length()
	var velocityLengthSquared = velocity.length_squared()
	var radius = position.length() # radius from object to primary (planet)
	var eccentricityVector = ((position * (velocityLengthSquared - gravitationalParameter / radius) - velocity * (position.dot(velocity)))  / gravitationalParameter)
	eccentricity = eccentricityVector.length()
	# Figure out what kind of orbit we are dealing with
	if Math.cmpflt(eccentricity, 0):
		type = ORBITTYPE_CIRCULAR
	elif Math.cmpflt(eccentricity, 1):
		type = ORBITTYPE_PARABOLIC
		print("Parabolic")
	elif eccentricity < 1:
		type = ORBITTYPE_ELLIPTICAL
	else:
		type = ORBITTYPE_HYPERBOLIC

	# Compute semiMajorAxis
	match type:
		ORBITTYPE_CIRCULAR, ORBITTYPE_ELLIPTICAL, ORBITTYPE_HYPERBOLIC:
			#var mechanicalEnergy = velocityLengthSquared / 2 - gravitationalParameter / radius
			#assert(mechanicalEnergy < 0)
			#semiMajorAxis = -gravitationalParameter / (2 * mechanicalEnergy) 
			semiMajorAxis = 1 / (2 / radius - velocityLengthSquared / gravitationalParameter)
		_:
			semiMajorAxis = NAN
	
	inclinationAngle = acos(angularMomentum.y / angularMomentum.length())
	var inclinationIsZero  = Math.cmpflt(inclinationAngle, 0)
	if inclinationIsZero:
		omega = 0
		if type == ORBITTYPE_CIRCULAR:
			argumentOfPeriapsis = 0
		else:
			argumentOfPeriapsis = acos(eccentricityVector.x / eccentricity)
	else:
		omega = acos(nodeVector.x / nodeVectorLength)
		if nodeVector.z > 0:
			omega = TAU - omega
		argumentOfPeriapsis = acos(nodeVector.dot(eccentricityVector) / (nodeVectorLength * eccentricity))
	if type == ORBITTYPE_CIRCULAR:
		if inclinationIsZero:
			trueAnomalyAngle = acos(position.x / radius)
			if velocity.x > 0:
				trueAnomalyAngle = TAU - trueAnomalyAngle
		else:
			trueAnomalyAngle = acos(nodeVector.dot(position) / (nodeVectorLength * radius))
			if nodeVector.dot(velocity) > 0:
				trueAnomalyAngle = TAU - trueAnomalyAngle
	else:
		if eccentricityVector.y < 0:
			argumentOfPeriapsis = TAU - argumentOfPeriapsis
		trueAnomalyAngle = acos(eccentricityVector.dot(position) / (eccentricity * radius))
		if position.dot(velocity) < 0:
			trueAnomalyAngle = TAU - trueAnomalyAngle
	
	# Compute pariapsisDistance
	match type:
		ORBITTYPE_CIRCULAR:
			pariapsisDistance = semiMajorAxis
		ORBITTYPE_HYPERBOLIC, ORBITTYPE_ELLIPTICAL:
			pariapsisDistance = semiMajorAxis * (1 - eccentricity)
		ORBITTYPE_PARABOLIC:
			pariapsisDistance = radius / (2 * (1 + cos(trueAnomalyAngle)))
		_:
			semiMajorAxis = NAN

func _compute_other_useful_stuff():
	match type:
		ORBITTYPE_CIRCULAR, ORBITTYPE_ELLIPTICAL:
			meanMotion = sqrt(gravitationalParameter / (semiMajorAxis * semiMajorAxis * semiMajorAxis))
		ORBITTYPE_PARABOLIC:
			meanMotion = sqrt(gravitationalParameter  / (2 * pariapsisDistance * pariapsisDistance * pariapsisDistance))
		ORBITTYPE_HYPERBOLIC:
			var invSemiMajorAxis = -semiMajorAxis
			meanMotion = sqrt(gravitationalParameter / (invSemiMajorAxis * invSemiMajorAxis * invSemiMajorAxis))
		_:
			meanMotion = NAN
	period = _computePeriod()
	trueAnomalyTime = get_travel_time(0, trueAnomalyAngle)
	
	# Compute semiLatusRectum
	match type:
		ORBITTYPE_CIRCULAR:
			semiLatusRectum = semiMajorAxis
		ORBITTYPE_ELLIPTICAL, ORBITTYPE_HYPERBOLIC:
			semiLatusRectum = semiMajorAxis * (1 - eccentricity * eccentricity)
		ORBITTYPE_PARABOLIC:
			semiLatusRectum = 2 * pariapsisDistance
		_:
			semiLatusRectum = NAN

func _compute_flight_path_info():
	# isLoop
	match type:
		ORBITTYPE_CIRCULAR, ORBITTYPE_ELLIPTICAL:
			isLoop = true
		_:
			isLoop = false
	# start and end values
	match type:
		ORBITTYPE_CIRCULAR, ORBITTYPE_ELLIPTICAL:
			startValue = 0
			endValue = TAU
		ORBITTYPE_PARABOLIC:
			var infAngle = PI - PI / 8 # This is just a guess
			startValue = -infAngle
			endValue = infAngle
		ORBITTYPE_HYPERBOLIC:
			var infAngle = acos(-1 / eccentricity)
			startValue = -infAngle
			endValue = infAngle
		_:
			startValue = 0
			endValue = 0
	# Path Time
	match type:
		ORBITTYPE_CIRCULAR, ORBITTYPE_ELLIPTICAL:
			pathTime = period
		ORBITTYPE_PARABOLIC, ORBITTYPE_HYPERBOLIC:
			pathTime = get_travel_time(startValue, endValue)
		_:
			pathTime = 0
	
# This function assumes the primary (planet or sun) is at origin
# This assumes velocity and position are realtive to the planet
func compute(gravitationalParameter : float, position : Vector3, velocity: Vector3):
	_compute_orbital_parameters(gravitationalParameter, position, velocity)
	_compute_other_useful_stuff()
	_compute_flight_path_info()


func compute_eccentric_anomaly_from_true_anomaly(trueAnomalyAngle: float):
	match type:
		ORBITTYPE_CIRCULAR:
			return trueAnomalyAngle
		ORBITTYPE_ELLIPTICAL:
			var cosOfTrueAnomalyAngle = cos(trueAnomalyAngle)
			return acos((eccentricity + cosOfTrueAnomalyAngle) / (1 + eccentricity * cosOfTrueAnomalyAngle))
		ORBITTYPE_PARABOLIC:
			return tan(trueAnomalyAngle / 2)
		ORBITTYPE_HYPERBOLIC:
			var cosOfTrueAnomalyAngle = cos(trueAnomalyAngle)
			var onePETCTAA = 1 + eccentricity * cosOfTrueAnomalyAngle
			if Math.cmpflt(onePETCTAA, 0):
				return NAN
			return Math.acosh((eccentricity + cosOfTrueAnomalyAngle) / onePETCTAA)
		_:
			return NAN

func compute_mean_anomaly_from_true_anomaly(trueAnomalyAngle: float):
	var normalizedAnomalyAngle = Math.normalizeAngle(trueAnomalyAngle)
	var eccentricAnomaly = compute_eccentric_anomaly_from_true_anomaly(trueAnomalyAngle)
	var meanAnomaly
	match type:
		ORBITTYPE_CIRCULAR:
			meanAnomaly = eccentricAnomaly
		ORBITTYPE_ELLIPTICAL:
			meanAnomaly = eccentricAnomaly - eccentricity * sin(eccentricAnomaly)
		ORBITTYPE_PARABOLIC:
			meanAnomaly = eccentricAnomaly + eccentricAnomaly * eccentricAnomaly * eccentricAnomaly / 3
		ORBITTYPE_HYPERBOLIC:
			meanAnomaly = eccentricity * sinh(eccentricAnomaly) - eccentricAnomaly
		_:
			meanAnomaly =  NAN
	if normalizedAnomalyAngle.angle > PI:
		meanAnomaly = TAU - meanAnomaly

	return meanAnomaly + normalizedAnomalyAngle.base

# The inverse function was hard, so I brute forced the solution
func compute_anomaly_from_mean_anomaly(meanAnomalyAngle: float, maxIterations: int = 30, estimationError: float = 0.00085):
	var normalizedMeanAnomalyAngle = Math.normalizeAngle(meanAnomalyAngle)
	var target = normalizedMeanAnomalyAngle.angle
	var isAbovePI = target > PI
	if isAbovePI:
		target = TAU - target
	
	#print("meanAnomalyAngle: ", meanAnomalyAngle, ", orbitCount: ", orbitCount, ", remainder: ", remainder)
	var maxAngle : float = PI
	var minAngle : float = 0
	var count : int = 0
	var computedTrueAnomaly = 0
	var lastCheckedAngle : float = 0
	while count < maxIterations and not Math.cmpflt(computedTrueAnomaly, target, estimationError):
		lastCheckedAngle = (maxAngle + minAngle) / 2
		computedTrueAnomaly = compute_mean_anomaly_from_true_anomaly(lastCheckedAngle)
		#print("count: ", count, ", maxAngle: ", maxAngle, ", minAngle: ", minAngle, ", lastCheckedAngle: ", lastCheckedAngle, ", computedTrueAnomaly: ", computedTrueAnomaly)
		if target < computedTrueAnomaly:
			maxAngle = lastCheckedAngle
		else:
			minAngle = lastCheckedAngle
		count += 1

	if isAbovePI:
		lastCheckedAngle = TAU - lastCheckedAngle
		
	return lastCheckedAngle + normalizedMeanAnomalyAngle.base

func get_travel_value(trueAnomaly0: float, deltaTime: float):
	var meanAnomaly0 = compute_mean_anomaly_from_true_anomaly(trueAnomaly0)
	var meanAnomaly1 = meanMotion * deltaTime + meanAnomaly0
	var trueAnomaly1 = compute_anomaly_from_mean_anomaly(meanAnomaly1)
	#var time = compute_time_using_anomaly(trueAnomaly0, trueAnomaly1)
	#print("deltaTime: ", deltaTime)
	#print("trueAnomaly0: ", trueAnomaly0, ", meanAnomaly0: ", meanAnomaly0)
	#print("meanAnomalyN: ", trueAnomaly1, ", meanAnomalyN: ", meanAnomaly1)
	#print("time: ", time)
	
	return trueAnomaly1

func get_travel_time(trueAnomaly0: float, trueAnomaly1: float):
	var meanAnomaly0 = compute_mean_anomaly_from_true_anomaly(trueAnomaly0)
	var meanAnomaly1 = compute_mean_anomaly_from_true_anomaly(trueAnomaly1)
	return (meanAnomaly1 - meanAnomaly0) / meanMotion

func test_mean_anomaly_func():
	print("== Test ==")
	for a in range(0, 720, 30):
		var ar = deg2rad(a)
		var ma = compute_mean_anomaly_from_true_anomaly(ar)
		var car = compute_anomaly_from_mean_anomaly(ma)
		print("a: ", ar, ", meanAnomaly: ", ma, ", inverseSolutions: ", car, ", diff: ", car - ar)

func get_at(value: float): 
	var orbitPoint = OrbitPoint.new()
	orbitPoint.compute(self, value)
	return orbitPoint



